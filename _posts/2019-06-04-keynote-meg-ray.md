---
title: "Keynote: Meg Ray"
date: 2019-06-04 00:00:00 -0400
image: /uploads/posts/meg-ray.jpg
excerpt_separator: <!--more-->
---
Meg Ray is a freelance educational consultant. She was the founding Teacher in Residence at Cornell
Tech were she was responsible for the implementation and design of a coaching program for K-8 CS
teachers in New York City schools. <!--more--> Meg served as a writer/advisor for the national CSTA
K-12 CS Standards, the CSTA/ISTE Computer Science Educator Standards, and the K12 CS Framework. She
is an experienced high school computer science teacher and special educator as well as a
graduate-level instructor at Hunter College. Previously, Meg created the Raspberry-Pi-in-a-box
program for Cornell Tech and directed the design of an online middle school Python programming
course for Codesters. Her research on CS teacher training and CS instruction for students with
disabilities is published in academic journals and conference proceedings. Meg gave the keynote at
the Education Summit at Pycon 2019 and is the author of the forthcoming introduction to Python book
Code this Game!
